"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ENTYPOEIDOSLOOKUP = exports.ICAOCOUNTRIESLOOKUP = exports.ORGLOOKUP = exports.getTitlos = exports.getTitlosAdeiaArithmos = exports.getEponymoOnomaHmGennisisYTX = exports.getTitlosDiabatirioArithmos = exports.getTitlosAMA = exports.getTitlosBebaiosiArithmos = void 0;
const soapClient_js_1 = __importDefault(require("./soapClient.js"));
const gsis_audit_record_db_1 = require("@digigov-oss/gsis-audit-record-db");
const config_json_1 = __importDefault(require("./config.json"));
const getParameters = (overrides) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c, _d, _e;
    const endpoint = (_a = overrides === null || overrides === void 0 ? void 0 : overrides.endpoint) !== null && _a !== void 0 ? _a : "";
    const prod = (_b = overrides === null || overrides === void 0 ? void 0 : overrides.prod) !== null && _b !== void 0 ? _b : false;
    const auditInit = (_c = overrides === null || overrides === void 0 ? void 0 : overrides.auditInit) !== null && _c !== void 0 ? _c : {};
    const auditStoragePath = (_d = overrides === null || overrides === void 0 ? void 0 : overrides.auditStoragePath) !== null && _d !== void 0 ? _d : "/tmp";
    const auditEngine = (_e = overrides === null || overrides === void 0 ? void 0 : overrides.auditEngine) !== null && _e !== void 0 ? _e : new gsis_audit_record_db_1.FileEngine(auditStoragePath);
    const wsdl = prod == true ? config_json_1.default.prod.wsdl : config_json_1.default.test.wsdl;
    const auditRecord = yield (0, gsis_audit_record_db_1.generateAuditRecord)(auditInit, auditEngine);
    if (!auditRecord)
        throw new Error('Audit record is not initialized');
    return { auditRecord, endpoint, wsdl };
});
/**
 * @param input GetTitlosBebaiosiArithmosInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
*/
const getTitlosBebaiosiArithmos = (input, user, pass, overrides) => __awaiter(void 0, void 0, void 0, function* () {
    const { auditRecord, endpoint, wsdl } = yield getParameters(overrides);
    try {
        const s = new soapClient_js_1.default(wsdl, user, pass, auditRecord, endpoint);
        const output = yield s.getTitle(input, "getTitlosBebaiosiArithmos");
        return { kedResponse: output,
            auditRecord: auditRecord };
    }
    catch (error) {
        throw (error);
    }
});
exports.getTitlosBebaiosiArithmos = getTitlosBebaiosiArithmos;
/**
 *
 * @param input GetTitlosAMAInput;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns Promise<GetTitleOutput>;
 */
const getTitlosAMA = (input, user, pass, overrides) => __awaiter(void 0, void 0, void 0, function* () {
    const { auditRecord, endpoint, wsdl } = yield getParameters(overrides);
    try {
        const s = new soapClient_js_1.default(wsdl, user, pass, auditRecord, endpoint);
        const output = yield s.getTitle(input, "getTitlosAMA");
        return { kedResponse: output,
            auditRecord: auditRecord };
    }
    catch (error) {
        throw (error);
    }
});
exports.getTitlosAMA = getTitlosAMA;
/**
 *
 * @param input GetTitlosDiabatirioArithmosInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
 */
const getTitlosDiabatirioArithmos = (input, user, pass, overrides) => __awaiter(void 0, void 0, void 0, function* () {
    const { auditRecord, endpoint, wsdl } = yield getParameters(overrides);
    try {
        const s = new soapClient_js_1.default(wsdl, user, pass, auditRecord, endpoint);
        const output = yield s.getTitle(input, "getTitlosDiabatirioArithmos");
        return { kedResponse: output,
            auditRecord: auditRecord };
    }
    catch (error) {
        throw (error);
    }
});
exports.getTitlosDiabatirioArithmos = getTitlosDiabatirioArithmos;
/**
 *
 * @param input GetEponymoOnomaHmGennisisYTXInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
 */
const getEponymoOnomaHmGennisisYTX = (input, user, pass, overrides) => __awaiter(void 0, void 0, void 0, function* () {
    const { auditRecord, endpoint, wsdl } = yield getParameters(overrides);
    try {
        const s = new soapClient_js_1.default(wsdl, user, pass, auditRecord, endpoint);
        const output = yield s.getTitle(input, "getEponymoOnomaHmGennisisYTX");
        return { kedResponse: output,
            auditRecord: auditRecord };
    }
    catch (error) {
        throw (error);
    }
});
exports.getEponymoOnomaHmGennisisYTX = getEponymoOnomaHmGennisisYTX;
/**
 *
 * @param input GetTitlosAdeiaArithmosInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
 */
const getTitlosAdeiaArithmos = (input, user, pass, overrides) => __awaiter(void 0, void 0, void 0, function* () {
    const { auditRecord, endpoint, wsdl } = yield getParameters(overrides);
    try {
        const s = new soapClient_js_1.default(wsdl, user, pass, auditRecord, endpoint);
        const output = yield s.getTitle(input, "getTitlosAdeiaArithmos");
        return { kedResponse: output,
            auditRecord: auditRecord };
    }
    catch (error) {
        throw (error);
    }
});
exports.getTitlosAdeiaArithmos = getTitlosAdeiaArithmos;
/**
 * auto detect the input type and set the appropriate value for the method
 * @param input GetTitlosBebaiosiArithmosInput | GetTitlosAMAInput | GetEponymoOnomaHmGennisisYTXInput | GetTitlosAdeiaArithmosInput
 * @param user
 * @param pass
 * @param overrides
 * @returns Promise<GetTitleOutput>;
 */
const getTitlos = (input, user, pass, overrides) => __awaiter(void 0, void 0, void 0, function* () {
    const { auditRecord, endpoint, wsdl } = yield getParameters(overrides);
    try {
        const s = new soapClient_js_1.default(wsdl, user, pass, auditRecord, endpoint);
        let method = "";
        if (input.hasOwnProperty('bebaiosiArithmos'))
            method = "getTitlosBebaiosiArithmos";
        if (input.hasOwnProperty('amaIka'))
            method = "getTitlosAMA";
        if (input.hasOwnProperty('diabatirioArithmos'))
            method = "getTitlosDiabatirioArithmos";
        if (input.hasOwnProperty('eponymo'))
            method = "getEponymoOnomaHmGennisisYTX";
        if (input.hasOwnProperty('adeiaArithmos'))
            method = "getTitlosAdeiaArithmos";
        if (method == "")
            throw new Error('Input type not recognized');
        const output = yield s.getTitle(input, method);
        return { kedResponse: output,
            auditRecord: auditRecord };
    }
    catch (error) {
        throw (error);
    }
});
exports.getTitlos = getTitlos;
var types_js_1 = require("./types.js");
Object.defineProperty(exports, "ORGLOOKUP", { enumerable: true, get: function () { return types_js_1.ORGLOOKUP; } });
Object.defineProperty(exports, "ICAOCOUNTRIESLOOKUP", { enumerable: true, get: function () { return types_js_1.ICAOCOUNTRIESLOOKUP; } });
Object.defineProperty(exports, "ENTYPOEIDOSLOOKUP", { enumerable: true, get: function () { return types_js_1.ENTYPOEIDOSLOOKUP; } });
exports.default = exports.getTitlos;
