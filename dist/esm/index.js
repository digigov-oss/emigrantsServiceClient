import soapClient from './soapClient.js';
import { generateAuditRecord, FileEngine } from '@digigov-oss/gsis-audit-record-db';
import config from './config.json';
const getParameters = async (overrides) => {
    const endpoint = overrides?.endpoint ?? "";
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {};
    const auditStoragePath = overrides?.auditStoragePath ?? "/tmp";
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord)
        throw new Error('Audit record is not initialized');
    return { auditRecord, endpoint, wsdl };
};
/**
 * @param input GetTitlosBebaiosiArithmosInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
*/
export const getTitlosBebaiosiArithmos = async (input, user, pass, overrides) => {
    const { auditRecord, endpoint, wsdl } = await getParameters(overrides);
    try {
        const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        const output = await s.getTitle(input, "getTitlosBebaiosiArithmos");
        return { kedResponse: output,
            auditRecord: auditRecord };
    }
    catch (error) {
        throw (error);
    }
};
/**
 *
 * @param input GetTitlosAMAInput;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns Promise<GetTitleOutput>;
 */
export const getTitlosAMA = async (input, user, pass, overrides) => {
    const { auditRecord, endpoint, wsdl } = await getParameters(overrides);
    try {
        const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        const output = await s.getTitle(input, "getTitlosAMA");
        return { kedResponse: output,
            auditRecord: auditRecord };
    }
    catch (error) {
        throw (error);
    }
};
/**
 *
 * @param input GetTitlosDiabatirioArithmosInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
 */
export const getTitlosDiabatirioArithmos = async (input, user, pass, overrides) => {
    const { auditRecord, endpoint, wsdl } = await getParameters(overrides);
    try {
        const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        const output = await s.getTitle(input, "getTitlosDiabatirioArithmos");
        return { kedResponse: output,
            auditRecord: auditRecord };
    }
    catch (error) {
        throw (error);
    }
};
/**
 *
 * @param input GetEponymoOnomaHmGennisisYTXInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
 */
export const getEponymoOnomaHmGennisisYTX = async (input, user, pass, overrides) => {
    const { auditRecord, endpoint, wsdl } = await getParameters(overrides);
    try {
        const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        const output = await s.getTitle(input, "getEponymoOnomaHmGennisisYTX");
        return { kedResponse: output,
            auditRecord: auditRecord };
    }
    catch (error) {
        throw (error);
    }
};
/**
 *
 * @param input GetTitlosAdeiaArithmosInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
 */
export const getTitlosAdeiaArithmos = async (input, user, pass, overrides) => {
    const { auditRecord, endpoint, wsdl } = await getParameters(overrides);
    try {
        const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        const output = await s.getTitle(input, "getTitlosAdeiaArithmos");
        return { kedResponse: output,
            auditRecord: auditRecord };
    }
    catch (error) {
        throw (error);
    }
};
/**
 * auto detect the input type and set the appropriate value for the method
 * @param input GetTitlosBebaiosiArithmosInput | GetTitlosAMAInput | GetEponymoOnomaHmGennisisYTXInput | GetTitlosAdeiaArithmosInput
 * @param user
 * @param pass
 * @param overrides
 * @returns Promise<GetTitleOutput>;
 */
export const getTitlos = async (input, user, pass, overrides) => {
    const { auditRecord, endpoint, wsdl } = await getParameters(overrides);
    try {
        const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        let method = "";
        if (input.hasOwnProperty('bebaiosiArithmos'))
            method = "getTitlosBebaiosiArithmos";
        if (input.hasOwnProperty('amaIka'))
            method = "getTitlosAMA";
        if (input.hasOwnProperty('diabatirioArithmos'))
            method = "getTitlosDiabatirioArithmos";
        if (input.hasOwnProperty('eponymo'))
            method = "getEponymoOnomaHmGennisisYTX";
        if (input.hasOwnProperty('adeiaArithmos'))
            method = "getTitlosAdeiaArithmos";
        if (method == "")
            throw new Error('Input type not recognized');
        const output = await s.getTitle(input, method);
        return { kedResponse: output,
            auditRecord: auditRecord };
    }
    catch (error) {
        throw (error);
    }
};
export { ORGLOOKUP, ICAOCOUNTRIESLOOKUP, ENTYPOEIDOSLOOKUP, } from './types.js';
export default getTitlos;
