import { AuditRecord, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
import type { GetTitlosBebaiosiArithmosInput, GetTitlosAMAInput, GetTitlosDiabatirioArithmosInput, GetEponymoOnomaHmGennisisYTXInput, GetTitlosAdeiaArithmosInput } from './types.js';
import type { GetTitleOutput, KEDInput } from './types.js';
/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {string} endpoint - The endpoint to connect to
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
/**
 * @param input GetTitlosBebaiosiArithmosInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
*/
export declare const getTitlosBebaiosiArithmos: (input: GetTitlosBebaiosiArithmosInput, user: string, pass: string, overrides?: Overrides) => Promise<GetTitleOutput>;
/**
 *
 * @param input GetTitlosAMAInput;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns Promise<GetTitleOutput>;
 */
export declare const getTitlosAMA: (input: GetTitlosAMAInput, user: string, pass: string, overrides?: Overrides) => Promise<GetTitleOutput>;
/**
 *
 * @param input GetTitlosDiabatirioArithmosInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
 */
export declare const getTitlosDiabatirioArithmos: (input: GetTitlosDiabatirioArithmosInput, user: string, pass: string, overrides?: Overrides) => Promise<GetTitleOutput>;
/**
 *
 * @param input GetEponymoOnomaHmGennisisYTXInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
 */
export declare const getEponymoOnomaHmGennisisYTX: (input: GetEponymoOnomaHmGennisisYTXInput, user: string, pass: string, overrides?: Overrides) => Promise<GetTitleOutput>;
/**
 *
 * @param input GetTitlosAdeiaArithmosInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
 */
export declare const getTitlosAdeiaArithmos: (input: GetTitlosAdeiaArithmosInput, user: string, pass: string, overrides?: Overrides) => Promise<GetTitleOutput>;
/**
 * auto detect the input type and set the appropriate value for the method
 * @param input GetTitlosBebaiosiArithmosInput | GetTitlosAMAInput | GetEponymoOnomaHmGennisisYTXInput | GetTitlosAdeiaArithmosInput
 * @param user
 * @param pass
 * @param overrides
 * @returns Promise<GetTitleOutput>;
 */
export declare const getTitlos: (input: KEDInput, user: string, pass: string, overrides?: Overrides) => Promise<GetTitleOutput>;
export { ORGLOOKUP, ICAOCOUNTRIESLOOKUP, ENTYPOEIDOSLOOKUP, ENTYPOEIDOS, ICAO, ORG, } from './types.js';
export { GetTitlosBebaiosiArithmosInput, GetTitlosAMAInput, GetEponymoOnomaHmGennisisYTXInput, GetTitlosAdeiaArithmosInput } from './types.js';
export { GetTitlosBebaiosiArithmosResponse, GetTitlosAMAResponse, GetTitlosDiabatirioArithmosResponse, GetEponymoOnomaHmGennisisYTXResponse, GetTitlosAdeiaArithmosResponse } from './types.js';
export { GetTitleOutput } from './types.js';
export { GetEponymoOnomaHmGennisisYTXReturn, GetTitlosAdeiaArithmosReturn, ErrorRecord, NomimosTitlos, EgkyraEggrafa } from './types.js';
export default getTitlos;
