import thesoap from 'soap';
let soap = thesoap;
try {
    soap = require('soap');
}
catch (error) {
    //my hackish way to make soap work on both esm and cjs
    //theshoap on esm is undefined
    //On esm require is not defined, however on cjs require can be used.
    //So we try to use require and if it fails we use the thesoap module
}
/**
 * SOAP client for retrieveInfoByAFM
 * @class Soap
 * @description SOAP client for retrieveInfoByAFM
 * @param {string} wsdl - The URL of the SOAP service
 * @param {string} username
 * @param {string} password
 * @param {AuditRecord} auditRecord
 * @param {string} endpoint
 */
class Soap {
    _wsdl;
    _username;
    _password;
    _auditRecord;
    _endpoint;
    constructor(wsdl, username, password, auditRecord, endpoint) {
        this._wsdl = wsdl;
        this._username = username;
        this._password = password;
        this._auditRecord = auditRecord;
        this._endpoint = endpoint;
    }
    async init() {
        try {
            const client = await soap.createClientAsync(this._wsdl, {
                wsdl_headers: {
                    'Authorization': 'Basic ' + Buffer.from(`${this._username}:${this._password}`).toString('base64'),
                },
            });
            if (this._endpoint) {
                client.setEndpoint(this._endpoint);
            }
            return client;
        }
        catch (e) {
            throw e;
        }
    }
    async getTitle(input, method = "") {
        try {
            const client = await this.init();
            var options = {
                hasNonce: true,
                actor: 'actor'
            };
            var wsSecurity = new soap.WSSecurity(this._username, this._password, options);
            client.setSecurity(wsSecurity);
            const auditRecord = this._auditRecord;
            let result = {};
            switch (method) {
                case "getTitlosBebaiosiArithmos":
                    const args = {
                        auditRecord: auditRecord,
                        getTitlosBebaiosiArithmosInputRecord: input,
                    };
                    result = await client.getTitlosBebaiosiArithmosAsync(args);
                    break;
                case "getTitlosAMA":
                    const args2 = {
                        auditRecord: auditRecord,
                        getTitlosAMAInputRecord: input,
                    };
                    result = await client.getTitlosAMAAsync(args2);
                    break;
                case "getTitlosDiabatirioArithmos":
                    const args3 = {
                        auditRecord: auditRecord,
                        getTitlosDiabatirioArithmosInputRecord: input,
                    };
                    result = await client.getTitlosDiabatirioArithmosAsync(args3);
                    break;
                case "getEponymoOnomaHmGennisisYTX":
                    const args4 = {
                        auditRecord: auditRecord,
                        getEponymoOnomaHmGennisisYTXInputRecord: input,
                    };
                    result = await client.getEponymoOnomaHmGennisisYTXAsync(args4);
                    break;
                case "getTitlosAdeiaArithmos":
                    const args5 = {
                        auditRecord: auditRecord,
                        getTitlosAdeiaArithmosInputRecord: input,
                    };
                    result = await client.getTitlosAdeiaArithmosAsync(args5);
                    break;
                default:
                    break;
            }
            return result[0];
        }
        catch (e) {
            throw e;
        }
    }
}
export default Soap;
