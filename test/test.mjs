import { getTitlos } from '../dist/esm/index.js';
import config from './config.json';
import inspect from 'object-inspect';
const test = async () => {
   
   //Try compinations to check the service in full.
   const inputs = [
    {
        //getTitlosBebaiosiArithmos
        aitisiOrganismos: "GSIS",
        bebaiosiArithmos: "2981832"
    },
    {
        //getTitlosAMA
        aitisiOrganismos: "GSIS",
        amaIka: "7763058",
        etosGennhshs: 1983
   },
   {
      //getTitlosDiabatirioArithmos
        aitisiOrganismos: "GGPS",
        diabatirioArithmos: "PU104015",
   },
   {
        //getEponymoOnomaHmGennisisYTX
        aitisiOrganismos: "GSIS",
        eponymo: "HARDAN",
        onoma: "ZENA",
        hmeromhniaGennhshs: "1991-06-05",
   },
   {
        //getTitlosAdeiaArithmos
        aitisiOrganismos: "GGPS",
        adeiaArithmos: "111164",
        entypoEidosId: "1",
   },
   {
       //getTitlosAdeiaArithmos error in entypoEidosId
       aitisiOrganismos: "GGPS",
       adeiaArithmos: "222264",
       entypoEidosId: "99",
   },
   {
      //getTitlosAdeiaArithmos error in adeiaArithmos
      aitisiOrganismos: "GGPS",
      adeiaArithmos: "000000",
      entypoEidosId: "1",
   },

 ]
   //loop on inputs array
    for (const input of inputs) {
    try {
                    const kedResponse = await getTitlos (input, config.user, config.pass);
                    console.log(inspect(kedResponse,{depth:10,indent:"\t"}));

    } catch (error) {
        console.log(error);
    }
    }
}

test();