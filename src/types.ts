import { AuditRecord } from '@digigov-oss/gsis-audit-record-db';

type TYear         = `${number}${number}${number}${number}`;
type TMonth        = `${number}${number}`;
type TDay          = `${number}${number}`;
type THours        = `${number}${number}`;
type TMinutes      = `${number}${number}`;
type TSeconds      = `${number}${number}`;
type TMilliseconds = `${number}${number}${number}`;
export type TDateISODate = `${TYear}-${TMonth}-${TDay}`;
export type TDateISOTime = `${THours}:${TMinutes}:${TSeconds}.${TMilliseconds}`;
export type TDateISO = `${TDateISODate}T${TDateISOTime}Z`;

type Enumerate<N extends number, Acc extends number[] = []> = Acc['length'] extends N
  ? Acc[number]
  : Enumerate<N, [...Acc, Acc['length']]>
type IntRange<F extends number, T extends number> = Exclude<Enumerate<T>, Enumerate<F>>

export type ORG = IntRange<1, 53>|99;
export const ORGLOOKUP = {
1: "ΔΑΜ ΑΘΗΝΩΝ Α | ΔΑΜ ΚΕΝΤΡΙΚΟΥ ΤΟΜΕΑ & ΔΥΤ. ΑΤΤΙΚΗΣ",
2: "ΔΑΜ ΒΟΡΕΙΟΥ ΤΟΜΕΑ & ΑΝΑΤΟΛ.ΑΤΤΙΚΗΣ",
3: "ΔΑΜ ΝΟΤΙΟΥ ΤΟΜΕΑ, ΠΕΙΡΑΙΩΣ & ΝΗΣΩΝ",
4: "ΔΑΜ ΛΕΣΒΟΥ",
5: "ΤΑΔ ΣΑΜΟΥ",
6: "ΤΑΔ ΧΙΟΥ",
7: "ΔΑΜ ΑΧΑΪΑΣ",
8: "ΤΑΔ ΑΙΤΩΛΟΑΚΑΡΝΑΝΙΑΣ",
9: "ΤΑΔ ΗΛΕΙΑΣ",
10: "ΤΑΔ ΚΟΖΑΝΗΣ",
11: "ΤΑΔ ΓΡΕΒΕΝΩΝ",
12: "ΤΑΔ ΚΑΣΤΟΡΙΑΣ",
13: "ΤΑΔ ΦΛΩΡΙΝΑΣ",
14: "ΔΑΜ ΙΩΑΝΝΙΝΩΝ",
15: "ΤΑΔ ΘΕΣΠΡΩΤΙΑΣ",
16: "ΤΑΔ ΠΡΕΒΕΖΑΣ",
17: "ΤΑΔ ΑΡΤΑΣ",
18: "ΔΑΜ ΛΑΡΙΣΑΣ",
19: "ΤΑΔ ΚΑΡΔΙΤΣΑΣ",
20: "ΤΑΔ ΜΑΓΝΗΣΙΑΣ",
21: "ΤΑΔ ΤΡΙΚΑΛΩΝ",
22: "ΤΑΔ ΚΕΡΚΥΡΑΣ",
23: "ΤΑΔ ΖΑΚΥΝΘΟΥ",
24: "ΤΑΔ ΚΕΦΑΛΛΗΝΙΑΣ",
25: "ΤΑΔ ΛΕΥΚΑΔΑΣ",
26: "ΔΑΜ ΘΕΣΣΑΛΟΝΙΚΗΣ",
27: "ΤΑΔ ΗΜΑΘΙΑΣ",
28: "ΤΑΔ ΚΙΛΚΙΣ",
29: "ΤΑΔ ΠΕΛΛΑΣ",
30: "ΤΑΔ ΠΙΕΡΙΑΣ",
31: "ΤΑΔ ΣΕΡΡΩΝ",
32: "ΤΑΔ ΧΑΛΚΙΔΙΚΗΣ",
33: "ΔΑΜ ΗΡΑΚΛΕΙΟΥ",
34: "ΤΑΔ ΛΑΣΙΘΙΟΥ",
35: "ΤΑΔ ΡΕΘΥΜΝΗΣ",
36: "ΤΑΔ Ν.ΧΑΝΙΩΝ",
37: "ΤΑΔ ΔΩΔΕΚΑΝΗΣΟΥ",
38: "ΤΑΔ ΚΥΚΛΑΔΩΝ",
39: "ΤΑΔ ΑΡΚΑΔΙΑΣ",
40: "ΤΑΔ ΑΡΓΟΛΙΔΑΣ",
41: "ΤΑΔ ΚΟΡΙΝΘΙΑΣ",
42: "ΤΑΔ ΛΑΚΩΝΙΑΣ",
43: "ΤΑΔ ΜΕΣΣΗΝΙΑΣ",
44: "ΤΑΔ ΦΘΙΩΤΙΔΑΣ",
45: "ΤΑΔ ΒΟΙΩΤΙΑΣ",
46: "ΤΑΔ ΕΥΒΟΙΑΣ",
47: "ΤΑΔ ΕΥΡΥΤΑΝΙΑΣ",
48: "ΤΑΔ ΦΩΚΙΔΑΣ",
49: "ΤΑΔ ΕΒΡΟΥ",
50: "ΤΑΔ ΔΡΑΜΑΣ",
51: "ΤΑΔ ΚΑΒΑΛΑΣ",
52: "ΤΑΔ ΞΑΝΘΗΣ",
53: "ΔΑΜ ΡΟΔΟΠΗΣ",
99: "ΚΕΝΤΡΙΚΗ ΥΠΗΡΕΣΙΑ ΥΜΑ"
}
//convert ORGLOOKUP to array
//export const ORGLOOKUPARRAY = Object.entries(ORGLOOKUP).map(([key, value]) => ({ key, value }));

// the following list is used to convert ICAO codes to country names, 
// it is based on 8265/2/08 REV 2 EU COUNCIL REGULATION (EC) No 95/93
// for VISA applications
export const ICAOCOUNTRIESLOOKUP = {
    "ABW":"ΑΡΟΥΜΠΑ",
    "AFG":"ΑΦΓΑΝΙΣΤΑΝ",
    "AGO":"ΑΝΓΚΟΛΑ",
    "AIA":"ΑΝΓΚΟΥΪΛΑ",
    "ALB":"ΑΛΒΑΝΙΑ",
    "AND":"ΑΝΔΟΡΑ",
    "ANT":"ΟΛΛΑΝΔΙΚΕΣ ΑΝΤΙΛΕΣ",
    "ARE":"ΗΝΩΜΕΝΑ ΑΡΑΒΙΚΑ ΕΜΙΡΑΤΑ",
    "ARG":"ΑΡΓΕΝΤΙΝΗ",
    "ARM":"ΑΡΜΕΝΙΑ",
    "ASM":"ΣΑΜΟΑ ΑΜΕΡΙΚΑΝΙΚΗ",
    "ATA":"ΑΝΤΑΡΚΤΙΚΗ",
    "ATF":"ΓΑΛΛΙΚΕΣ ΝΟΤΙΕΣ ΚΤΗΣΕΙΣ (ΕΙΡ. ΩΚ.)",
    "ATG":"ΑΝΤΙΓΟΥΑ & ΜΠΑΡΜΠΟΥΝΤΑ",
    "AUS":"ΑΥΣΤΡΑΛΙΑ",
    "AUT":"ΑΥΣΤΡΙΑ",
    "AZE":"ΑΖΕΡΜΠΑΪΤΖΑΝ",
    "BDI":"ΜΠΟΥΡΟΥΝΤΙ",
    "BEL":"ΒΕΛΓΙΟ",
    "BEN":"ΜΠΕΝΙΝ",
    "BFA":"ΜΠΟΥΡΚΙΝΑ ΦΑΣΟ",
    "BGD":"ΜΠΑΝΓΚΛΑΝΤΕΣ",
    "BGR":"ΒΟΥΛΓΑΡΙΑ",
    "BHR":"ΜΠΑΧΡΕΪΝ",
    "BHS":"ΜΠΑΧΑΜΕΣ",
    "BIH":"ΒΟΣΝΙΑ-ΕΡΖΕΓΟΒΙΝΗ",
    "BLR":"ΛΕΥΚΟΡΩΣΙΑ",
    "BLZ":"ΜΠΕΛΙΖΕ",
    "BMU":"ΒΕΡΜΟΥΔΕΣ",
    "BOL":"ΒΟΛΙΒΙΑ",
    "BRA":"ΒΡΑΖΙΛΙΑ",
    "BRB":"ΜΠΑΡΜΠΑΝΤΟΣ",
    "BRN":"ΜΠΡΟΥΝΕΪ ΝΤΑΡΟΥΣΑΛΑΜ",
    "BTN":"ΜΠΟΥΤΑΝ",
    "BVT":"ΝΗΣΟΣ ΜΠΟΥΒΕ",
    "BWA":"ΜΠΟΤΣΟΥΑΝΑ",
    "BYS":"Belarus SSR",
    "CAF":"ΚΕΝΤΡΟΑΦΡΙΚΑΝΙΚΗ ΔΗΜΟΚΡΑΤΙΑ",
    "CAN":"ΚΑΝΑΔΑΣ",
    "CCK":"Ν. ΚΟΚΟΣ (ΚΙΛΙΝΚΓ)",
    "CHE":"ΕΛΒΕΤΙΑ",
    "CHL":"ΧΙΛΗ",
    "CHN":"ΚΙΝΑ",
    "CIV":"ΑΚΤΗ ΕΛΕΦΑΝΤΟΣΤΟΥ",
    "CMR":"ΚΑΜΕΡΟΥΝ",
    "COD":"ΛΑΪΚΗ ΔΗΜΟΚΡΑΤΙΑ ΤΟΥ ΚΟΝΓΚΟ",
    "COG":"ΚΟΝΓΚΟ",
    "COK":"Ν. ΚΟΥΚ",
    "COL":"ΚΟΛΟΜΒΙΑ",
    "COM":"ΚΟΜΟΡΕΣ",
    "CPV":"ΠΡΑΣΙΝΟ ΑΚΡΩΤΗΡΙΟ",
    "CRC":"International Committee of the Red Cross",
    "CRI":"ΚΟΣΤΑ ΡΙΚΑ",
    "CSK":"Czechoslovakia",
    "CUB":"ΚΟΥΒΑ",
    "CXR":"Ν. ΧΡΙΣΤΟΥΓΕΝΝΩΝ",
    "CYM":"ΝΗΣΟΣ ΚΑΫΜΑΝ",
    "CYP":"ΚΥΠΡΟΣ",
    "CZE":"ΤΣΕΧΙΑ",
    "DEU":"ΓΕΡΜΑΝΙΑ",
    "DJI":"ΤΖΙΜΠΟΥΤΙ",
    "DMA":"ΔΟΜΙΝΙΚΑ",
    "DNK":"ΔΑΝΙΑ",
    "DOM":"ΔΟΜΙΝΙΚΑΝΗ ΔΗΜΟΚΡΑΤΙΑ",
    "DZA":"ΑΛΓΕΡΙΑ",
    "ECU":"ΙΣΗΜΕΡΙΝΟΣ",
    "EGY":"ΑΙΓΥΠΤΟΣ",
    "ERI":"ΕΡΥΘΡΑΙΑ",
    "ESH":"ΔΥΤΙΚΗ ΣΑΧΑΡΑ",
    "ESP":"ΙΣΠΑΝΙΑ",
    "EST":"ΕΣΘΟΝΙΑ",
    "ETH":"ΑΙΘΙΟΠΙΑ",
    "FIN":"ΦΙΝΛΑΝΔΙΑ",
    "FJI":"ΦΙΤΖΙ",
    "FLK":"Ν. ΦΟΚΛΑΝΤ",
    "FRA":"ΓΑΛΛΙΑ",
    "FRO":"Ν. ΦΕΡΟΕΣ",
    "FSM":"ΜΙΚΡΟΝΗΣΙΑ",
    "FXX":"ΜΗΤΡΟΠΟΛΙΤΙΚΗ ΓΑΛΛΙΑ",
    "GAB":"ΓΚΑΜΠΟΝ",
    "GBD":"ΗΝ. ΒΑΣ. Μ. ΒΡΕΤΑΝΙΑΣ- ΕΞΗΡΤΗΜ.ΚΤΗΣΕΙΣ",
    "GBN":"ΗΝ. ΒΑΣ. Μ. ΒΡΕΤΑΝΙΑΣ- ΕΞΩΤΕΡΙΚΟΥ",
    "GBO":"ΗΝ. ΒΑΣ. Μ. ΒΡΕΤΑΝΙΑΣ- ΚΤΗΣΕΙΣ ΕΞΩΤΕΡΙΚΟΥ",
    "GBP":"ΗΝ. ΒΑΣ. Μ. ΒΡΕΤΑΝΙΑΣ- ΠΡΟΣΤΑΤ. ΠΡΟΣΩΠΟ",
    "GBR":"ΗΝ. ΒΑΣ. Μ.ΒΡΕΤΑΝΙΑΣ",
    "GBS":"ΗΝ. ΒΑΣ. Μ. ΒΡΕΤΑΝΙΑΣ- ΥΠΗΚΟΟΣ",
    "GEO":"ΓΕΩΡΓΙΑ",
    "GHA":"ΓΚΑΝΑ",
    "GIB":"ΓΙΒΡΑΛΤΑΡ",
    "GIN":"ΓΟΥΪΝΕΑ",
    "GLP":"ΓΟΥΑΔΕΛΟΥΠΗ",
    "GMB":"ΓΚΑΜΠΙΑ",
    "GNB":"ΓΟΥΪΝΕΑ ΜΠΙΣΑΟ",
    "GNQ":"ΙΣΗΜΕΡΙΝΗ ΓΟΥΪΝΕΑ",
    "GRC":"ΕΛΛΑΔΑ",
    "GRD":"ΓΡΕΝΑΔΑ",
    "GRL":"ΓΡΟΙΛΑΝΔΙΑ",
    "GTM":"ΓΟΥΑΤΕΜΑΛΑ",
    "GUF":"ΓΑΛΛΙΚΗ ΓΟΥΪΑΝΑ",
    "GUM":"Ν. ΓΚΟΥΑΜ",
    "GUY":"ΓΟΥΪΑΝΑ",
    "HKG":"ΧΟΝΓΚ ΚΟΝΓΚ",
    "HMD":"Ν. ΧΕΡΝΤ & ΜΑΚΝΤΟΝΑΛΤ",
    "HND":"ΟΝΔΟΥΡΑ",
    "HRV":"ΚΡΟΑΤΙΑ",
    "HTI":"ΑΪΤΗ",
    "HUN":"ΟΥΓΓΑΡΙΑ",
    "IDN":"ΙΝΔΟΝΗΣΙΑ",
    "IND":"ΙΝΔΙΑ",
    "IOT":"ΒΡΕΤΑΝ. ΚΤΗΣΕΙΣ ΙΝΔΙΚΟΥ ΩΚΕΑΝΟΥ",
    "IRL":"ΙΡΛΑΝΔΙΑ",
    "IRN":"ΙΡΑΝ",
    "IRQ":"ΙΡΑΚ",
    "ISL":"ΙΣΛΑΝΔΙΑ",
    "ISR":"ΙΣΡΑΗΛ",
    "ITA":"ΙΤΑΛΙΑ",
    "JAM":"ΤΖΑΜΑΪΚΑ",
    "JOR":"ΙΟΡΔΑΝΙΑ",
    "JPN":"ΙΑΠΩΝΙΑ",
    "KAZ":"ΚΑΖΑΚΣΤΑΝ",
    "KEN":"ΚΕΝΥΑ",
    "KGZ":"ΚΙΡΓΙΖΙΑ",
    "KHM":"ΚΑΜΠΟΤΖΗ",
    "KIR":"Ν. ΚΙΡΙΜΠΑΤΙ",
    "KNA":"ΑΓΙΟΣ ΧΡΙΣΤΟΦΟΡΟΣ ΚΑΙ ΝΕΒΙΣ",
    "KOR":"ΚΟΡΕΑ (ΝΟΤΙΟΣ)",
    "KWT":"ΚΟΥΒΕΪΤ",
    "LAO":"ΛΑΟΣ",
    "LBN":"ΛΙΒΑΝΟΣ",
    "LBR":"ΛΙΒΕΡΙΑ",
    "LBY":"ΛΙΒΥΗ",
    "LCA":"ΣΑΝΤΑ ΛΟΥΤΣΙΑ",
    "LIE":"ΛΙΧΤΕΝΣΤΑΪΝ",
    "LKA":"ΣΡΙ ΛΑΝΚΑ",
    "LSO":"ΛΕΣΟΤΟ",
    "LTU":"ΛΙΘΟΥΑΝΙΑ",
    "LUX":"ΛΟΥΞΕΜΒΟΥΡΓΟ",
    "LVA":"ΛΕΤΟΝΙΑ",
    "MAC":"ΜΑΚΑΟ",
    "MAR":"ΜΑΡΟΚΟ",
    "MCO":"ΜΟΝΑΚΟ",
    "MDA":"ΜΟΛΔΑΒΙΑ",
    "MDG":"ΜΑΔΑΓΑΣΚΑΡΗ",
    "MDV":"ΜΑΛΔΙΒΕΣ",
    "MEX":"ΜΕΞΙΚΟ",
    "MHL":"Ν. ΜΑΡΣΑΛ",
    "MKD":"ΒΟΡΕΙΑ ΜΑΚΕΔΟΝΙΑ",
    "MLI":"ΜΑΛΙ",
    "MLT":"ΜΑΛΤΑ",
    "MMR":"ΜΥΑΝΜΑΡ (ΒΙΡΜΑΝΙΑ)",
    "MNE":"ΜΑΥΡΟΒΟΥΝΙΟ",
    "MNG":"ΜΟΓΓΟΛΙΑ",
    "MNP":"Ν. ΒΟΡΕΙΕΣ ΜΑΡΙΑΝΕΣ",
    "MOZ":"ΜΟΖΑΜΒΙΚΗ",
    "MRT":"ΜΑΥΡΙΤΑΝΙΑ",
    "MSR":"ΜΟΝΤΣΕΡΑΤ",
    "MTQ":"ΜΑΡΤΙΝΙΚΑ",
    "MUS":"ΜΑΥΡΙΚΙΟΣ",
    "MWI":"ΜΑΛΑΟΥΪ",
    "MYS":"ΜΑΛΑΙΣΙΑ",
    "MYT":"ΜΑΫΟΤ",
    "NAM":"ΝΑΜΙΜΠΙΑ",
    "NCL":"ΝΕΑ ΚΑΛΙΔΟΝΙΑ",
    "NER":"ΝΙΓΗΡΑΣ",
    "NFK":"Ν. ΝΟΡΦΟΛΚ",
    "NGA":"ΝΙΓΗΡΙΑ",
    "NIC":"ΝΙΚΑΡΑΓΟΥΑ",
    "NIU":"ΝΙΟΥΕ",
    "NLD":"ΟΛΛΑΝΔΙΑ",
    "NOR":"ΝΟΡΒΗΓΙΑ",
    "NPL":"ΝΕΠΑΛ",
    "NRU":"ΝΑΟΥΡΟΥ",
    "NTZ":"ΟΥΔΕΤΕΡΗ ΖΩΝΗ",
    "NZL":"ΝΕΑ ΖΗΛΑΝΔΙΑ",
    "OMN":"ΟΜΑΝ",
    "PAK":"ΠΑΚΙΣΤΑΝ",
    "PAN":"ΠΑΝΑΜΑΣ",
    "PCN":"Ν. ΠΙΤΚΕΡΝ",
    "PER":"ΠΕΡΟΥ",
    "PHL":"ΦΙΛΙΠΠΙΝΕΣ",
    "PLW":"ΠΑΛΑΟΥ",
    "PNG":"ΠΑΠΟΥΑ-ΝΕΑ ΓΟΥΪΝΕΑ",
    "POL":"ΠΟΛΩΝΙΑ",
    "PRI":"ΠΟΡΤΟ ΡΙΚΟ",
    "PRK":"Λ.Δ. ΤΗΣ ΚΟΡΕΑΣ ( ΒΟΡΕΙΟΣ)",
    "PRT":"ΠΟΡΤΟΓΑΛΙΑ",
    "PRY":"ΠΑΡΑΓΟΥΑΗ",
    "PSE":"ΠΑΛΑΙΣΤΙΝΗ",
    "PYF":"ΓΑΛΛΙΚΗ ΠΟΛΥΝΗΣΙΑ",
    "QAT":"ΚΑΤΑΡ",
    "REU":"Ν. ΡΕΫΟΥΝΙΟΝ (ΕΝΩΣΕΩΣ)",
    "ROU":"ΡΟΥΜΑΝΙΑ",
    "RUS":"ΡΩΣΙΑ",
    "RWA":"ΡΟΥΑΝΤΑ",
    "SAU":"ΣΑΟΥΔΙΚΗ ΑΡΑΒΙΑ",
    "SCG":"ΣΕΡΒΙΑ-ΜΑΥΡΟΒΟΥΝΙΟ",
    "SDN":"ΣΟΥΔΑΝ",
    "SEN":"ΣΕΝΕΓΑΛΗ",
    "SGP":"ΣΙΝΓΚΑΠΟΥΡΗ",
    "SGS":"ΝΟΤ. ΓΕΩΡΓΙΑ & Ν. ΣΑΝΤΟΥΪΤΣ",
    "SHN":"ΑΓ. ΕΛΕΝΗ",
    "SJM":"ΣΒΑΛΜΠΑΡΝΤ & Ν. ΧΟΥΑΝ ΜΑΫΕΝ",
    "SLB":"Ν. ΣΟΛΩΜΟΝΤΟΣ",
    "SLE":"ΣΙΕΡΑ ΛΕΟΝΕ",
    "SLV":"ΕΛ ΣΑΛΒΑΔΟΡ",
    "SMR":"ΑΓ.ΜΑΡΙΝΟΣ",
    "SOM":"ΣΟΜΑΛΙΑ",
    "SPM":"ΣΑΙΝΤ ΠΙΕΡ ΚΑΙ ΜΙΚΕΛΟΝ",
    "SRB":"ΣΕΡΒΙΑ",
    "STP":"ΣΑΟ ΤΟΜΕ & ΠΡΙΝΣΙΠΕ",
    "SUN":"ΣΟΒΙΕΤΙΚΗ ΕΝΩΣΗ",
    "SUR":"ΣΟΥΡΙΝΑΜ",
    "SVK":"ΣΛΟΒΑΚΙΑ",
    "SVN":"ΣΛΟΒΕΝΙΑ",
    "SWE":"ΣΟΥΗΔΙΑ",
    "SWZ":"ΣΟΥΑΖΙΛΑΝΔΗ",
    "SYC":"ΣΕΫΧΕΛΛΕΣ",
    "SYR":"ΣΥΡΙΑ",
    "TCA":"Ν. ΤΟΥΡΚΣ & ΚΑΪΚΟΣ",
    "TCD":"ΤΣΑΝΤ",
    "TGO":"ΤΟΓΚΟ",
    "THA":"ΤΑΪΛΑΝΔΗ",
    "TJK":"ΤΑΤΖΙΚΙΣΤΑΝ",
    "TKL":"ΤΟΚΕΛΑΟΥ",
    "TKM":"ΤΟΥΡΚΜΕΝΙΣΤΑΝ",
    "TMP":"ΑΝΑΤΟΛΙΚΟ ΤΙΜΟΡ",
    "TON":"ΤΟΓΚΑ",
    "TTO":"ΤΡΙΝΙΤΑΝΤ & ΤΟΜΠΑΓΚΟ",
    "TUN":"ΤΥΝΗΣΙΑ",
    "TUR":"ΤΟΥΡΚΙΑ",
    "TUV":"ΤΟΥΒΑΛΟΥ",
    "TWN":"ΤΑΪΒΑΝ",
    "TZA":"ΤΑΝΖΑΝΙΑ",
    "UGA":"ΟΥΓΚΑΝΤΑ",
    "UKR":"ΟΥΚΡΑΝΙΑ",
    "UMI":"ΗΠΑ - ΚΤΗΣΕΙΣ ΜΙΚ. ΝΗΣΩΝ ( ΕΙΡΗΝΙΚΟΣ)",
    "UNA":"ΟΗΕ - ΕΚΠΡΟΣΩΠΟΣ ΟΡΓΑΝΩΣΗΣ ΤΟΥ.",
    "UNO":"ΟΗΕ ΟΡΓΑΝΙΣΜΟΣ ΗΝ. ΕΘΝΩΝ",
    "UNR":"UNHCR",
    "URY":"ΟΥΡΟΥΓΟΥΑΗ",
    "USA":"ΗΝ. ΠΟΛΙΤΕΙΕΣ ΑΜΕΡΙΚΗΣ - ΗΠΑ",
    "UZB":"ΟΥΖΜΠΕΚΙΣΤΑΝ",
    "VAT":"ΠΟΛΗ ΤΟΥ ΒΑΤΙΚΑΝΟΥ (ΑΓΙΑ ΕΔΡΑ)",
    "VCT":"ΑΓ. ΒΙΚΕΝΤΙΟΣ & ΓΡΕΝΑΔΙΝΟΙ ΝΗΣΟΙ",
    "VEN":"ΒΕΝΕΖΟΥΕΛΑ",
    "VGB":"Ν. ΠΑΡΘΕΝΟΥ ( ΒΡΕΤΑΝ. ΚΤΗΣΗ)",
    "VIR":"Ν. ΠΑΡΘΕΝΟΥ ( ΑΜΕΡ. ΚΤΗΣΗ)",
    "VNM":"ΒΙΕΤΝΑΜ",
    "VUT":"ΒΑΝΟΥΑΤΟΥ",
    "WLF":"Ν. ΟΥΑΛΙΣ & ΦΟΥΤΟΥΝΑ",
    "WSM":"ΣΑΜΟΑ",
    "XXA":"ΠΡΟΣΩΠΟ ΧΩΡΙΣ ΥΠΗΚΟΟΤΗΤΑ ( §1, ΣΥΝΘΗΚΗ 1954)",
    "XXB":"ΠΡΟΣΦ. ΩΣ ΟΡΙΖΕΤΑΙ ΣΤΗΝ §1, ΣΥΝΘ. 1951 & ΠΡΩΤΟΚ. 1967",
    "XXC":"ΠΡΟΣΦΥΓΑΣ ΆΛΛΟΣ ΩΣ ΟΡΙΖΕΤΑΙ ΣΤΗΝ ΠΕΡΙΠΤ. ΧΧΒ (ΠΑΡΑΠΑΝΩ)",
    "XXD":"ΚΟΣΟΒΟ",
    "XXP":"Palestine",
    "XXT":"ΘΙΒΕΤ",
    "XXX":"ΑΚΑΘΟΡΙΣΤΗ",
    "XXY":"Palestinian nationality not recognized by all States (used in Germany)",
    "XYU":"Yugoslavia (before independence of the former constituent republics)",
    "YEM":"ΥΕΜΕΝΗ",
    "YMD":"Yemen (People's Democratic Republic of)",
    "YUG":"ΓΙΟΥΓΚΟΣΛΑΒΙΑ ( ΣΕΡΒΙΑ-ΜΑΥΡΟΒΟΥΝΙΟ)",
    "ZAF":"ΝΟΤΙΟΣ ΑΦΡΙΚΗ",
    "ZMB":"ΖΑΜΠΙΑ",
    "ZWE":"ΖΙΜΠΑΜΠΟΥΕ"

}

export type ICAO = keyof typeof ICAOCOUNTRIESLOOKUP;
export const ENTYPOEIDOSLOOKUP = {
             1:"ΒΙΝΙΕΤΑ",
             2:"ΒΕΒΑΙΩΣΗ ΚΑΤΑΘΕΣΗΣ",
             5:"ΔΕΛΤΙΟ ΔΙΑΜΟΝΗΣ",
             6:"ΔΕΛΤΙΟ ΜΟΝΙΜΗΣ ΔΙΑΜΟΝΗΣ",
             7:"ΕΙΔΙΚΗ ΒΕΒΑΙΩΣΗ ΝΟΜΙΜΗΣ ΔΙΑΜΟΝΗΣ",
             8:"ΑΥΤΟΤΕΛΕΣ ΕΓΓΡΑΦΟ"
}
export type ENTYPOEIDOS = 1|2|IntRange<5, 9>;

export type ErrorRecord = {
    errorCode:string;
    errorDescr:string;
}

export type NomimosTitlos = {
    adeiaArithmos: number;
    adeiaEkdoths: string;
    adeiaParathrhsh1: string;
    adeiaParathrhsh2: null|''|"ΑΝΑΝΕΩΣΗ"|"ΧΟΡΗΓΗΣΗ";
    adeiaTypos: string;
    cd: IntRange<1, 5>;
    diabathrioArithmos: string;
    diabathrioHmeromhniaLhkshs: TDateISO;
    entypoEidosId: number;
    eponymo: string;
    fakelosArithmos: number;
    fyloId: 1|2; // 1: Men, 2: Women 3:...
    hmeromhniaEkdoshs: TDateISO;
    hmeromhniaEnarkshsIsxyos?: null|string;
    hmeromhniaGennhshs: TDateISO;
    hmeromhniaLhkshsIsxyos: TDateISO;
    ithageneiaCode: ICAO;
    mhtrooId: number;
    nomimos: boolean; // false: illegal, true: legal
    onoma: string;
    org: ORG;
    patronymo: string;
    xoraGennhshsCode: ICAO; 
    errorMsg: string;
}

export type EgkyraEggrafa = {
    adeiaGid:string;
    afm:string;
    ama:string;
    diabathrioArithmos:string;
    eggrafoArithmos:number;
    eggrafoEidosId:number;
    eggrafoHmeromhniaEkdoshs:TDateISO;
    eggrafoHmeromhniaLhkshsIsxyos:TDateISO;
    eggrafoPerigrafh:string;
    ekdothsDieythynsh:string;
    eponymo:string;
    fyloId:number;
    hmeromhniaGennhshs:TDateISO;
    mhtronymo:string;
    mhtrooId:number;
    onoma:string;
    patronymo:string;
    teleytaioEggrafoArithmos:number;
    teleytaioEggrafoEidosId:number;
    xoraGennhshsCode:string;
    yphkoothtaCode:string;
}

/*titlosBebaiosiArithmos*/
export type GetTitlosBebaiosiArithmosInput = {
    aitisiOrganismos    : string;
    bebaiosiArithmos    : number;
}
export type GetTitlosBebaiosiArithmosRequest = {
    auditRecord: AuditRecord;
    getTitlosBebaiosiArithmosInputRecord: GetTitlosBebaiosiArithmosInput;
}

export type GetTitlosBebaiosiArithmosReturn = NomimosTitlos;

export type GetTitlosBebaiosiArithmosResponse = {
    getTitlosBebaiosiArithmosOutputRecord: {
        result:GetTitlosBebaiosiArithmosReturn;
    }
    callSequenceId: number;
    callSequenceDate: TDateISO;
    errorRecord:ErrorRecord | null;
}

/*titlosAMA*/
export type GetTitlosAMAInput = {
    aitisiOrganismos: string;
    amaIka: string;
    etosGennhshs: number;
}
export type GetTitlosAMARequest = {
        auditRecord: AuditRecord;
        getTitlosAMAInputRecord: GetTitlosAMAInput;
}

export type GetTitlosAMAReturn = NomimosTitlos;

export type GetTitlosAMAResponse = {
         getTitlosAMAOutputRecord:{
            result:GetTitlosAMAReturn;
         };
         callSequenceId: number;
         callSequenceDate: TDateISO;
         errorRecord:ErrorRecord | null;
}

/*titlosDiabatirioArithmos*/
export type GetTitlosDiabatirioArithmosInput = {
    aitisiOrganismos: string;
    diabatirioArithmos: string;
}
export type GetTitlosDiabatirioArithmosRequest = {
    auditRecord: AuditRecord;
    getTitlosDiabatirioArithmosInputRecord: GetTitlosDiabatirioArithmosInput;
}
export type GetTitlosDiabatirioArithmosReturn = NomimosTitlos;
export type GetTitlosDiabatirioArithmosResponse = {
    getTitlosDiabatirioArithmosOutputRecord:{
        result:GetTitlosDiabatirioArithmosReturn;
    };
    callSequenceId: number;
    callSequenceDate: TDateISO;
    errorRecord:ErrorRecord | null;
}
/*eponymoOnomaHmGennisisYTX*/
export type GetEponymoOnomaHmGennisisYTXInput = {
    aitisiOrganismos: string;
    eponymo: string;
    onoma: string;
    hmeromhniaGennhshs: string; //does not accept ISO date...
}
export type GetEponymoOnomaHmGennisisYTXRequest = {
    auditRecord: AuditRecord;
    getEponymoOnomaHmGennisisYTXInputRecord: GetEponymoOnomaHmGennisisYTXInput;
}
export type GetEponymoOnomaHmGennisisYTXReturn = {
    recordCount: number;
    items?:EgkyraEggrafa[]|null;
    errorMessage?:string;
}

export type GetEponymoOnomaHmGennisisYTXResponse = {
    getEponymoOnomaHmGennisisYTXOutputRecord:{
        result:GetEponymoOnomaHmGennisisYTXReturn;
    };
    callSequenceId: number;
    callSequenceDate: TDateISO;
    errorRecord:ErrorRecord | null;
}

/*titlosAdeiaArithmos*/
export type GetTitlosAdeiaArithmosInput = {
    aitisiOrganismos: string;
    adeiaArithmos: number;
    entypoEidosId: ENTYPOEIDOS;
}
export type GetTitlosAdeiaArithmosRequest = {
    auditRecord: AuditRecord;
    getTitlosAdeiaArithmosInputRecord: GetTitlosAdeiaArithmosInput;
}
export type GetTitlosAdeiaArithmosReturn = NomimosTitlos;
export type GetTitlosAdeiaArithmosResponse = {
    getTitlosAdeiaArithmosOutputRecord:{
        result:GetTitlosAdeiaArithmosReturn;
    };
    callSequenceId: number;
    callSequenceDate: TDateISO;
    errorRecord:ErrorRecord | null;
}

export type KEDInput = GetTitlosAMAInput | GetTitlosBebaiosiArithmosInput | GetTitlosDiabatirioArithmosInput | GetEponymoOnomaHmGennisisYTXInput | GetTitlosAdeiaArithmosInput;
export type KEDResponse = ErrorRecord | GetTitlosAMAResponse | GetTitlosBebaiosiArithmosResponse | GetTitlosDiabatirioArithmosResponse | GetEponymoOnomaHmGennisisYTXResponse | GetTitlosAdeiaArithmosResponse;
export type GetTitleOutput ={
     kedResponse: KEDResponse;
     auditRecord: AuditRecord;
}

