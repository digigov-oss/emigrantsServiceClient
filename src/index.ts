import soapClient from './soapClient.js';
import {generateAuditRecord, AuditRecord, FileEngine, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
import config from './config.json'; 
import type {GetTitlosBebaiosiArithmosInput,
        GetTitlosAMAInput,
        GetTitlosDiabatirioArithmosInput,
        GetEponymoOnomaHmGennisisYTXInput,
        GetTitlosAdeiaArithmosInput } from './types.js';
import type { GetTitlosBebaiosiArithmosResponse,
        GetTitlosAMAResponse,
        GetTitlosDiabatirioArithmosResponse,
        GetEponymoOnomaHmGennisisYTXResponse,
        GetTitlosAdeiaArithmosResponse} from './types.js';
import type { GetTitleOutput,KEDInput,KEDResponse } from './types.js';

/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {string} endpoint - The endpoint to connect to
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
 export type Overrides = {
    endpoint?:string;
    prod?:boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
 }

const getParameters = async (overrides?:Overrides) => {
    const endpoint = overrides?.endpoint ?? "";
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {} as AuditRecord;
    const auditStoragePath = overrides?.auditStoragePath ?? "/tmp"
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod==true? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord) throw new Error('Audit record is not initialized');
    return { auditRecord,endpoint,wsdl };
}

/**
 * @param input GetTitlosBebaiosiArithmosInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
*/
export const getTitlosBebaiosiArithmos = async (input:GetTitlosBebaiosiArithmosInput, user:string, pass:string, overrides?:Overrides):Promise<GetTitleOutput> => {
    const { auditRecord, endpoint, wsdl } = await getParameters(overrides);
    try {
         const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
         const output:GetTitlosBebaiosiArithmosResponse = await s.getTitle(input, "getTitlosBebaiosiArithmos") as GetTitlosBebaiosiArithmosResponse;
         return {kedResponse:output,
                auditRecord:auditRecord};
       } catch (error) {
           throw(error);
       }
}

/**
 * 
 * @param input GetTitlosAMAInput;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns Promise<GetTitleOutput>;
 */
 export const getTitlosAMA = async (input:GetTitlosAMAInput, user:string, pass:string, overrides?:Overrides):Promise<GetTitleOutput> => {
    const { auditRecord, endpoint, wsdl } = await getParameters(overrides);
    try {
         const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
         const output = await s.getTitle(input, "getTitlosAMA") as GetTitlosAMAResponse;
         return {kedResponse:output,
                auditRecord:auditRecord};
       } catch (error) {
           throw(error);
       }
}

/**
 * 
 * @param input GetTitlosDiabatirioArithmosInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
 */
export const getTitlosDiabatirioArithmos = async (input:GetTitlosDiabatirioArithmosInput, user:string, pass:string, overrides?:Overrides):Promise<GetTitleOutput> => {
        const { auditRecord, endpoint, wsdl } = await getParameters(overrides);
        try {
             const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
             const output = await s.getTitle(input, "getTitlosDiabatirioArithmos") as GetTitlosDiabatirioArithmosResponse;
             return {kedResponse:output,
                    auditRecord:auditRecord};
           } catch (error) {
               throw(error);
           }
}

/**
 * 
 * @param input GetEponymoOnomaHmGennisisYTXInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
 */
export const getEponymoOnomaHmGennisisYTX = async (input:GetEponymoOnomaHmGennisisYTXInput, user:string, pass:string, overrides?:Overrides):Promise<GetTitleOutput> => {
    const { auditRecord, endpoint, wsdl } = await getParameters(overrides);
    try {
            const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
            const output:GetEponymoOnomaHmGennisisYTXResponse = await s.getTitle(input, "getEponymoOnomaHmGennisisYTX") as GetEponymoOnomaHmGennisisYTXResponse;
            return {kedResponse:output,
                auditRecord:auditRecord};
        } catch (error) {
            throw(error);
        }
}
/**
 * 
 * @param input GetTitlosAdeiaArithmosInput;
 * @param user string;
 * @param pass string;
 * @param overrides Overrides;
 * @returns Promise<GetTitleOutput>;
 */
export const getTitlosAdeiaArithmos = async (input:GetTitlosAdeiaArithmosInput, user:string, pass:string, overrides?:Overrides):Promise<GetTitleOutput> => {
    const { auditRecord, endpoint, wsdl } = await getParameters(overrides);
    try {
            const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
            const output:GetTitlosAdeiaArithmosResponse = await s.getTitle(input, "getTitlosAdeiaArithmos") as GetTitlosAdeiaArithmosResponse;
            return {kedResponse:output,
                auditRecord:auditRecord};
        } catch (error) {
            throw(error);
        }
}

/**
 * auto detect the input type and set the appropriate value for the method
 * @param input GetTitlosBebaiosiArithmosInput | GetTitlosAMAInput | GetEponymoOnomaHmGennisisYTXInput | GetTitlosAdeiaArithmosInput
 * @param user 
 * @param pass 
 * @param overrides 
 * @returns Promise<GetTitleOutput>;
 */
export const getTitlos = async (input:KEDInput, user:string, pass:string, overrides?:Overrides):Promise<GetTitleOutput> => {
    const { auditRecord, endpoint, wsdl } = await getParameters(overrides);
    try {
            const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
            let method = "";
            if (input.hasOwnProperty('bebaiosiArithmos')) method = "getTitlosBebaiosiArithmos";
            if (input.hasOwnProperty('amaIka')) method = "getTitlosAMA";
            if (input.hasOwnProperty('diabatirioArithmos')) method = "getTitlosDiabatirioArithmos";
            if (input.hasOwnProperty('eponymo')) method = "getEponymoOnomaHmGennisisYTX";
            if (input.hasOwnProperty('adeiaArithmos')) method = "getTitlosAdeiaArithmos";
            if (method == "") throw new Error('Input type not recognized');
            const output:KEDResponse = await s.getTitle(input, method);
            return {kedResponse:output,
                    auditRecord:auditRecord};
        } catch (error) {
            throw(error);
        }
}

export {
        ORGLOOKUP,
        ICAOCOUNTRIESLOOKUP,
        ENTYPOEIDOSLOOKUP, 
        ENTYPOEIDOS, 
        ICAO,
        ORG,
} from './types.js';

export {
        GetTitlosBebaiosiArithmosInput,
        GetTitlosAMAInput,
        GetEponymoOnomaHmGennisisYTXInput,
        GetTitlosAdeiaArithmosInput } from './types.js';

export {
        GetTitlosBebaiosiArithmosResponse,
        GetTitlosAMAResponse,
        GetTitlosDiabatirioArithmosResponse,
        GetEponymoOnomaHmGennisisYTXResponse,
        GetTitlosAdeiaArithmosResponse} from './types.js';

export { GetTitleOutput } from './types.js';

export {
        GetEponymoOnomaHmGennisisYTXReturn,
        GetTitlosAdeiaArithmosReturn,
        ErrorRecord,
        NomimosTitlos,
        EgkyraEggrafa } from './types.js';

export default getTitlos;
