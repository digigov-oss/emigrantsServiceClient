import {
    ErrorRecord,
    GetTitlosBebaiosiArithmosResponse,
    GetTitlosAMAResponse,    
    GetTitlosDiabatirioArithmosResponse,
    GetEponymoOnomaHmGennisisYTXResponse,
    GetTitlosAdeiaArithmosResponse,
    KEDInput,
    KEDResponse
  } from './types.js'
import thesoap from 'soap';
let soap:any = thesoap;
try {
   soap = require('soap');
} catch (error) {
  //my hackish way to make soap work on both esm and cjs
  //theshoap on esm is undefined
  //On esm require is not defined, however on cjs require can be used.
  //So we try to use require and if it fails we use the thesoap module
}

import {AuditRecord} from '@digigov-oss/gsis-audit-record-db';

/**
 * SOAP client for retrieveInfoByAFM
 * @class Soap
 * @description SOAP client for retrieveInfoByAFM
 * @param {string} wsdl - The URL of the SOAP service
 * @param {string} username
 * @param {string} password
 * @param {AuditRecord} auditRecord
 * @param {string} endpoint
 */
class Soap {
  private _wsdl: string;
  private _username: string;
  private _password: string;
  private _auditRecord: AuditRecord;
  private _endpoint: string;


  constructor(wsdl: string, username: string, password: string,auditRecord: AuditRecord, endpoint: string) {
    this._wsdl = wsdl;
    this._username = username;
    this._password = password;
    this._auditRecord = auditRecord;
    this._endpoint = endpoint;
  }

  public async init() {
    try {
      const client = await soap.createClientAsync(this._wsdl, {
        wsdl_headers: {
          'Authorization': 'Basic ' + Buffer.from(`${this._username}:${this._password}`).toString('base64'),
        },
      });
      if (this._endpoint) {
        client.setEndpoint(this._endpoint);
      }
      return client;
    } catch (e) {
      throw e;
    }
  }

  public async getTitle(input:KEDInput,method:string=""):Promise<KEDResponse> {
    try {
      const client = await this.init();
      var options = {
        hasNonce: true,
        actor: 'actor'
      };
      var wsSecurity = new soap.WSSecurity(this._username, this._password, options)
      client.setSecurity(wsSecurity);
      const auditRecord = this._auditRecord;
      let result:any={};


      switch (method) {

        case "getTitlosBebaiosiArithmos":
            const args = {
                 auditRecord: auditRecord,
                 getTitlosBebaiosiArithmosInputRecord: input,
            }
            result = await client.getTitlosBebaiosiArithmosAsync(args) as GetTitlosBebaiosiArithmosResponse;
        break;

        case "getTitlosAMA":
            const args2 = {
                 auditRecord: auditRecord,
                 getTitlosAMAInputRecord: input,
            }
            result = await client.getTitlosAMAAsync(args2) as GetTitlosAMAResponse;
        break;

        case "getTitlosDiabatirioArithmos":
            const args3 = {
                 auditRecord: auditRecord,
                 getTitlosDiabatirioArithmosInputRecord: input,
            }
            result = await client.getTitlosDiabatirioArithmosAsync(args3) as GetTitlosDiabatirioArithmosResponse;
        break;

        case "getEponymoOnomaHmGennisisYTX":
            const args4 = {
                    auditRecord: auditRecord,
                    getEponymoOnomaHmGennisisYTXInputRecord: input,
            }
            result = await client.getEponymoOnomaHmGennisisYTXAsync(args4) as GetEponymoOnomaHmGennisisYTXResponse;
        break;

        case "getTitlosAdeiaArithmos":
            const args5 = {
                    auditRecord: auditRecord,
                    getTitlosAdeiaArithmosInputRecord: input,
            }
            result = await client.getTitlosAdeiaArithmosAsync(args5) as GetTitlosAdeiaArithmosResponse;
        break;

        default:
        break;
        }
      return result[0];
    } catch (e) {
      throw e;
    }
  }
}

export default Soap;